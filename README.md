[Capacitive Button]  
by 
[Mohamed Abdel Aziz]
Date:
[8/12/2016]

CONTENTS
I.      Purpose
II.	HOW TO CONNECT THE CIRCUIT
III.	MINIMUM SYSTEM REQUIREMENTS
IV.	KNOWN ISSUES AND WORKAROUNDS
V.	TECHNICAL SUPPORT


I.  Purpose
It just very simple project that i use two condactive matrial 
with two different resistors,So when you touch
the conductive matrial the led of it will turn on and            
if you touch it again the led turns off  


II. HOW TO CONNECT THE CIRCUIT


1. Get two conductive matrial (for me i used foil) 
2. Connect one of these conductive matrial with one mega ohm resistor 
3. Connect the other side of matrail to wire 
4. make same for second one but different resistor like 10 mega ohm
5. for first one coonect resistor to pin 2 on your arduino and wire to pin 3
6. for the second coonect resistor to pin 2 on your arduino and wire to pin 4
7. upload code to your arduino borad and test it.


III. MINIMUM SYSTEM REQUIREMENTS 


1. Twoconductive matrial ( i used foil you can use copper)
2. Wires
3. Arduino ( i used uno)
4. Two resistors ( i used 1 mega and 10 mega ohm)
5. leds and 220 ohm resistors for it
  


IV. KNOWN ISSUES AND WORK AROUNDS 
1. The value thats get on serial monitor changes if you change resistor value
2. If you put glass on condactive matrial then you touch it , the value will change so 
   make sure the value of tolal_1 and total_2 and test it.
3. see the difference between code 1 and code2.



V. TECHNICAL SUPPORT 
e-mail: mohamed.abdelaziz1994@yahoo.com